# encoding: utf-8
"""
cerbere.mapper.SMAPJPLHDF5File
==============================

Mapper class for JPL SMAP L2 HDF5 files

:copyright: Copyright 2017 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import copy
from datetime import datetime

import numpy

import cerbere.mapper.slices
from cerbere.mapper import hdf5file
from cerbere.datamodel import field
from cerbere.datamodel import variable

REFERENCE_TIME = datetime(2001, 1, 1, 0, 0, 0)

MS = "MS"
HS = "HS"


class SMAPJPLHDF5File(hdf5file.HDF5File):
    """Mapper class for SMAP JPL L2 HDF5 files"""

    def __init__(self,
                 url=None,
                 mode=hdf5file.READ_ONLY,
                 **kwargs):
        """
        Args:

        """
        hdf5file.HDF5File.__init__(self, url=url, mode=mode, **kwargs)

    def open(self,
             view=None, **kwargs):
        """Open the HDF5 file

        Args:
            view (dict, optional): a dictionary where keys are dimension names
                and values are slices. A view can be set on a file, meaning
                that only the subset defined by this view will be accessible.
                This view is expressed as any subset (see :func:`get_values`).
                For example::

                view = {'time':slice(0,0), 'lat':slice(200,300),
                'lon':slice(200,300)}

        Returns:
            an handler on the opened file
        """
        handler = hdf5file.HDF5File.open(
            self, view, **kwargs)
        return handler

    def get_native_fieldname(self, fieldname):
        """Return the native field name in the file format for a standard field
        such as geolocation fields (lat, lon, time, z) or any field which are
        exposed to users with a different name for some reason.
        .

        Used for internal purpose and should not be called directly.

        Args:
            fieldname (str): name of the field to translate into its native
                name

        Return:
            str: name of the corresponding field in the native file format.
        """
        if fieldname in ['lat', 'lon']:
            return ('/%s' % fieldname)
        elif fieldname == 'time':
            return "time"
        else:
            return fieldname

    def get_standard_fieldname(self, fieldname):
        """Return the equivalent standard field name for a fieldname in the
        file format

        Used for internal purpose and should not be called directly.

        Args:
            fieldname (str): name of the native geolocation field name

        Return:
            str: standard name of the corresponding  field in the native file
                format, if any (native name otherwise).
        """
        if fieldname == 'row_time':
            return "time"
        else:
            return fieldname

    def get_native_dimname(self, dimname):
        """Return the equivalent name in the native format for a standard
        dimension.

        This is a translation of the standard names to native ones. It is used
        for internal purpose only and should not be called directly.

        The standard dimension names are row, cell, time for
        :class:`~cerbere.datamodel.swath.Swath`

        Args:
            dimname (str): standard dimension name.

        Returns:
            str: return the native name for the dimension. Return `dimname` if
                the input dimension has no standard name.

        See Also:
            see :func:`get_standard_dimname` for the reverse operation
        """
        try:
            idx = ['cell', 'row', 'ambiguity'].index(dimname)
            return "smap_ambiguity_spd%d" % idx
        except:
            return dimname

    def get_standard_dimname(self, dimname):
        """
        Returns the equivalent standard dimension name for a
        dimension in the native format.

        This is a translation of the native names to standard ones. It is used
        for internal purpose and should not be called directly.

        To be derived when creating an inherited data mapper class. This is
        mandatory for geolocation dimensions which must be standard.

        Args:
            dimname (string): native dimension name

        Return:
            str: the (translated) standard name for the dimension. Return
            `dimname` if the input dimension has no standard name.

        See Also:
            see :func:`get_matching_dimname` for the reverse operation
        """
        if dimname[:-1] in ['row_time']:
            return 'row'
        else:
            return ['cell', 'row', 'ambiguity'][int(dimname[-1])]

    def get_dimensions(self, fieldname=None):
        """Return the dimension's standard names of a file or a field in the
        file.

        Args:
            fieldname (str): the name of the field from which to get the
                dimensions. For a geolocation field, use the cerbere standard
                name (time, lat, lon), though native field name will work too.

        Returns:
            tuple<str>: the standard dimensions of the field or file.
        """
        dims = list(hdf5file.HDF5File.get_dimensions(self, fieldname))
        if fieldname is not None:
            if fieldname == 'row_time':
                return ('row',)

            # re-order row and cells
            dims.insert(0, dims.pop(1))

        return tuple(dims)

    def read_field_attributes(self, fieldname):
        """Return the specific attributes of a field.

        Args:
            fieldname (str): name of the field.

        Returns:
            dict<string, string or number or datetime>: a dictionary where keys
                are the attribute names.
        """
        attrs = hdf5file.HDF5File.read_field_attributes(self, fieldname)

        # as scalar
        for att, val in attrs.items():
            if isinstance(val, numpy.ndarray) and len(val) == 1:
                attrs[att] = val[0]

        return attrs

    def read_field(self, fieldname):
        """
        Return the :class:`cerbere.field.Field` object corresponding to
        the requested fieldname.

        The :class:`cerbere.field.Field` class contains all the metadata
        describing a field (equivalent to a variable in netCDF).

        Args:
            fieldname (str): name of the field

        Returns:
            :class:`cerbere.field.Field`: the corresponding field object
        """
        if fieldname == 'time':
            # create a field for time
            vartime = variable.Variable(
                shortname='time',
                description='acquisition time',
                authority=self.get_naming_authority(),
                standardname='time'
            )
            fieldobj = field.Field(
                vartime,
                self.get_full_dimensions('lat'),
                datatype=numpy.dtype(numpy.int64),
                units=self.read_field('row_time').units
            )
            fieldobj.attach_storage(self.get_field_handler(fieldname))

        else:
            fieldobj = hdf5file.HDF5File.read_field(self, fieldname)

        return fieldobj

    def get_start_time(self):
        """Returns the minimum date of the file temporal coverage.

        Returns:
            datetime: start time of the data in file.
        """
        attr = self.read_global_attribute('time_coverage_start')
        return datetime.strptime(attr[:-5], "%Y-%jT%H:%M:%S")

    def get_end_time(self):
        """Returns the maximum date of the file temporal coverage.

        Returns:
            datetime: end time of the data in file.
        """
        attr = self.read_global_attribute('time_coverage_end')
        return datetime.strptime(attr[:-5], "%Y-%jT%H:%M:%S")

    def read_values(self, fieldname, slices=None):
        """Read the data of a field.

        Args:
            fieldname (str): name of the field which to read the data from

            slices (list of slice, optional): list of slices for the field if
                subsetting is requested. A slice must then be provided for each
                field dimension. The slices are relative to the opened view
                (see :func:open) if a view was set when opening the file.

        Return:
            MaskedArray: array of data read. Array type is the same as the
                storage type.
        """
        # reorder slices (to match (cell, row) storage instead of (row,cell))
        ordered_slices = None
        if slices is not None:
            ordered_slices = list(copy.copy(slices))
            ordered_slices.insert(0, ordered_slices.pop(1))
            ordered_slices = tuple(ordered_slices)

        if fieldname == 'time':
            # adjust slice with respect to view
            # time is only defined along-track
            dims = self._get_field_dimensions('lat')
            dimsizes = [self.get_dimsize(_) for _ in dims]
            newslices = cerbere.mapper.slices.get_absolute_slices(
                view=self.view,
                slices=ordered_slices,
                dimnames=dims,
                dimsizes=dimsizes
            )
            shape = cerbere.mapper.slices.get_shape_from_slice(newslices)
            newslices = [newslices[0]]

            total_seconds = hdf5file.HDF5File.read_values(self,
                                                          'row_time',
                                                          newslices)

            # reshape to (row, cell) array
            total_seconds = numpy.resize(total_seconds,
                                         (shape[0], shape[1])
                                         ).transpose()

            return total_seconds
        else:
            print "XXXXXXXXXXXXXX", fieldname, self.get_dimensions(fieldname), ordered_slices
            if len(self.get_dimensions(fieldname)) == 2:
                transposition = (1, 0)
            else:
                transposition = (1, 0, 2)
            return hdf5file.HDF5File.read_values(
                self, fieldname, ordered_slices
            ).transpose(transposition)

    def get_product_version(self):
        """return the product version"""
        return self.read_global_attribute('product_version')

    def get_orbit_number(self):
        """In the case of a satellite orbit file, returns the orbit number.

        Returns:
            int: the orbit number
        """
        return int(self.read_global_attribute('REVNO'))

    def get_collection_id(self):
        if self.read_global_attribute('processing_level') == "L2B":
            return "SMAP_JPL_L2B_SSS_CAP_V3"
        raise Exception("Product collection unknown for %s" %
                        self.read_global_attribute('processing_level'))
