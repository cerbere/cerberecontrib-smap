"""
Author: Antoine Grouazel
SMAP L3 winds from REMSS are daily gridded product
/home/cerdata/provider/remss/smap/l3/v00.1/daily/data
"""

from cerbere.mapper.ncfile import NCFile
from cerbere.datamodel.field import Field
from cerbere.datamodel.variable import Variable
from collections import OrderedDict
import netCDF4
from .. import READ_ONLY, WRITE_NEW
import numpy as np
import sys
import logging
import datetime
import os

import pdb
DEFAULT_TIME_UNITS = 'seconds since 2010-01-01 00:00:00'
VIRTUALFIELD_DESCR = {
#                       'ascending_wind_speed': 'wind speed for ascending acquisitions',
                      'wind_speed': 'wind speed',
                      'time': 'time',
#                       'descending_wind_speed': 'wind speed for descending acquisitions',
#                         'ascending_time': 'time for ascending nodes',
#                         'descending_time':'time for descending nodes',
#                       'wind_direction': 'wind direction (where the wind is blowing)'
                      }
VIRTUALFIELD_STDNAME = {  'wind_speed': 'wind_speed',
                        'time': 'time',
                        
#                         'ascending_wind_speed': 'wind_speed',
#                         'descending_wind_speed': 'wind_speed',
#                         'ascending_time': 'time',
#                         'descending_time':'time',
#                       'wind_direction': 'wind_to_direction'
                      }
VIRTUALFIELD_UNITS = {
                      
                'wind_speed': 'm s-1',
                'time': DEFAULT_TIME_UNITS,
                      
#                 'ascending_wind_speed': 'm s-1',
#                 'descending_wind_speed': 'm s-1',
#                 'ascending_time': DEFAULT_TIME_UNITS,
#                 'descending_time': DEFAULT_TIME_UNITS
#                 'wind_direction': 'degrees'
                }



class SMAPL3REMSSNCFile(NCFile):
    def __init__(self, url=None,node='ascending', mode=READ_ONLY,ncformat='NETCDF4', **kwargs):
        super(SMAPL3REMSSNCFile,self).__init__(url=url,mode=mode,ncformat=ncformat,depths=True,center_on_greenwhich=False,**kwargs)
        if self._handler is None:
            
            self._handler = self.get_handler()
        self.node = node # ascending or descending
        nodes_indices = {'ascending':0,'descending':1}
        self.node_idx = nodes_indices[node]
        _ = self.read_values('time')
        return  
    
    def get_fieldnames(self):
        '''
        Returns the list of geophysical fields stored for the feature

        Excludes the geolocation fields in the returned list
        '''
        geophyvars = super(SMAPL3REMSSNCFile, self).get_fieldnames()
#         geophyvars.extend(["ascending_wind_speed", "descending_wind_speed",ascending_time])
#         geophyvars.extend(VIRTUALFIELD_STDNAME.keys())
        geophyvars.extend(['wind_speed','time'])
        geophyvars.remove('minute')
        geophyvars.remove('wind')
        #geophyvars.remove('height')   
        return geophyvars
    
    def read_values(self, fieldname, slices=None):
        if fieldname == 'wind_speed':
            native_fieldname = 'wind'
        elif fieldname == 'time':
            native_fieldname = 'minute'  
#         if fieldname =='ascending_wind_speed':
#             native_fieldname = 'wind'
#             idx = 0
#         elif fieldname =='descending_wind_speed':
#             native_fieldname = 'wind'
#             idx = 1
#         elif fieldname == 'ascending_time':
#             idx = 0
#             native_fieldname = 'minute'
#         elif fieldname == 'descending_time':
#             idx = 1
#             native_fieldname = 'minute'
        else:
            
            native_fieldname = fieldname
        if native_fieldname == 'wind':
            if slices is None:
                slices = OrderedDict([('node',slice(self.node_idx,self.node_idx+1,None))])
            else:
                slices.append(('node',slice(self.node_idx,self.node_idx+1,None)))
        elif native_fieldname =='minute':
            if slices is None:
                slices = OrderedDict([('node',slice(self.node_idx,self.node_idx+1,None))])
            else:
                slices.append(('node',slice(self.node_idx,self.node_idx+1,None)))
#             res = super(SMAPL3REMSSNCFile, self).read_values(native_fieldname,slices=slices)
        res = super(SMAPL3REMSSNCFile, self).read_values(native_fieldname,slices=slices)
        res = res.squeeze()
        if native_fieldname == 'wind':
            res = np.ma.masked_where(res>60.,res,copy=True) #agrouaze arbitrarily decision since there are values up to 90m/s on ice
            res = res[self.maskdata]
            logging.debug('shape of the %s: %s',fieldname,res.shape)
        elif native_fieldname == 'minute':
            #RSS_smap_wind_daily_2017_01_31_v00.1
            date_of_file = datetime.datetime.strptime(os.path.basename(self._url),'RSS_smap_wind_daily_%Y_%m_%d_v00.1.nc')
            self._timeunits = 'seconds since %s'%date_of_file.strftime('%Y-%m-%d %H:%M:%S')
            logging.debug('units: %s',self._timeunits)
#             masko = res.mask
            res = np.array(res,dtype=float)
            
            logging.debug('raw values of times type= %s',type(res))
            self.maskdata = res!=-9999
            logging.debug('Nber of good data: %s/%s (%s%%)',np.sum(self.maskdata),res.size,100*np.sum(self.maskdata)/res.size)
            logging.debug('maskdata: %s',self.maskdata)
            logging.debug('dtype minutes: %s ',res.dtype)
#             res[res==-9999] = np.nan 
            logging.debug('type minutes: %s ex: %s',type(res),res)
            logging.debug('nbre of times before filter: %s',res.size)
            res = res[self.maskdata]
            logging.debug('nbre of times after filter: %s',res.size)
            res = res*60. #to put in seconds
#             res = netCDF4.num2date(res*60.,self._timeunits)
#             res = np.ma.array(res,mask=masko)
#             pdb.set_trace()
#             res = datetime.timedelta(minutes=res)+date_of_file
            logging.debug('shape date of the file: %s',res.shape)
        elif native_fieldname in ['lon','lat']:
#             tmplon = super(SMAPL3REMSSNCFile, self).read_values('lon',slices=slices)
#             tmplat = super(SMAPL3REMSSNCFile, self).read_values('lat',slices=slices)
            tmplon = super(SMAPL3REMSSNCFile, self).read_values('lon',slices=None)
            tmplat = super(SMAPL3REMSSNCFile, self).read_values('lat',slices=None)
#             tmptimes = self.read_values('time', slices)
            
            lonf,latf = np.meshgrid(tmplon,tmplat)
            lonf = lonf[self.maskdata]
            latf = latf[self.maskdata]
            if native_fieldname == 'lon':
                res = lonf
            else:
                res = latf
        return res
    
#     def get_matching_dimname(self, geodimname):
#         """
#         Return the equivalent name in the current data mapper for a standard
#         feature dimension, or None if not found.
#         """
#         matching = {
#                 'row': 'rows',
#                 'cell': 'cols',
#                 'time': 'time',
# #                 'vec_1': 'vec',
# #                 'vec_2': 'vec',
#                 }
#         if geodimname in matching:
#             return matching[geodimname]
#         else:
#             return None
    
    def read_field(self, fieldname):
        """
        Return the field, without its values.

        Actual values can be retrieved with read_values() method.
        """
#         if fieldname in ['ascending_wind_speed', 'descending_wind_speed','ascending_time',"descending_time","lon","lat"]:
        if fieldname in ['wind_speed','time',"lon","lat"]:
            # create a virtual field
#             y_dim = self.get_dimsize('lat')
#             x_dim = self.get_dimsize('lon')
            variable = Variable(
                    shortname=fieldname,
                    description=VIRTUALFIELD_DESCR[fieldname],
                    authority=self.get_naming_authority(),
                    standardname=VIRTUALFIELD_STDNAME[fieldname]
                    )
            if fieldname != 'time':
                uu = VIRTUALFIELD_UNITS[fieldname]
            else:
                uu = self._timeunits
            field = Field(
                    variable,
                    OrderedDict([('time',np.sum(self.maskdata) ),
                                 #('z', 1),
#                                  ('y', y_dim),
#                                  ('x', x_dim)
                                 ]),
                    datatype=np.dtype(np.float32),
                    units=uu
                    )
            field.attach_storage(self.get_field_handler(fieldname))
        else:
            raise Exception('there is no other fields to read')

        return field
    
    def get_end_time(self):
        date_of_file = datetime.datetime.strptime(os.path.basename(self._url),'RSS_smap_wind_daily_%Y_%m_%d_v00.1.nc')
        
        return date_of_file+datetime.timedelta(hours=23,minutes=59,seconds=59)
    
    def get_start_time(self):
        date_of_file = datetime.datetime.strptime(os.path.basename(self._url),'RSS_smap_wind_daily_%Y_%m_%d_v00.1.nc')
        logging.debug('smap l3 mapper: get_start_time =%s',date_of_file)
        return date_of_file
    
    def get_bbox(self):
        if self._mapped_data is None:
            self.read_values('lon')
#             lons = 
        lonmin = self._mapped_data['lon'].min()
        latmin = self._mapped_data['lat'].min()
        lonmax = self._mapped_data['lon'].max()
        latmax = self._mapped_data['lat'].max()
        return lonmin,latmin,lonmax,latmax