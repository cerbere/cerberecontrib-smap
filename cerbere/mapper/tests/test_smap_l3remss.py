from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import logging
import datetime
import netCDF4
import collections
# from cerbere.datamodel.grid import Grid
from cerbere.datamodel.swath import Swath
from cerbere.datamodel.trajectory import Trajectory
from cerbere.mapper.smapl3remssncfile import SMAPL3REMSSNCFile

def test_minutes(handler):
    """
    je veux voir si je converti bien les minutes de la journee en dt
    """
    ascendingtimes = handler.read_values('time')
    print 'mapped times',ascendingtimes.min(),ascendingtimes.max()
    units = handler.read_field('time').units
    nc = netCDF4.Dataset(handler._url)
    mintes = nc.variables['minute'][:]
    mintes = np.ma.masked_where(mintes==-9999,mintes)
    print 'minutes raw',mintes.min(),mintes.max()
    print 'converted mapped times min = ',netCDF4.num2date(ascendingtimes.min(),units)
#     print datetime.datetime.strftime(ascendingtimes.min(),units)
    

def test_loadind(nc):
    lons = nc.read_values('lon')
    print lons.shape
    print "max= %s, min %s"%(lons.max(),lons.min())
    lats = nc.read_values('lat')
    print lats.shape
    print 'fields,',nc.get_fieldnames()
    a_times = nc.read_values('time')
    a_time_field = nc.read_field('time')
#     print 'ascending times',a_times.shape,'max = %s min = %s,'%(a_times.max(),a_times.min())
    print 'times, first value',a_times[0],type(a_times[0])
    print 'end time',nc.get_end_time()
    print 'start time,',nc.get_start_time()
    d_ws = nc.read_values('wind_speed')
    a_times_num = netCDF4.date2num(a_times,a_time_field.units)
    print 'wind_speed',d_ws.shape,d_ws.max()
    if True:
        mW=-179
        mE=179
        mN=70
        mS=-60
        plt.hold('true')
        plt.title('')
        m = Basemap(projection='merc',llcrnrlat=mS,urcrnrlat=mN,llcrnrlon=mW, urcrnrlon=mE,resolution='c')
        m.drawmapboundary(fill_color='#85A6D9')
        m.drawcoastlines(color='#6D5F47', linewidth=.4)
        m.fillcontinents(color='grey',lake_color='#85A6D9')
        #     m.drawcountries(color='r')
        #m.shadedrelief()
        meridians = np.arange(mW,mE,np.around((mE-mW)/10.0))
        m.drawmeridians(meridians,labels=[0,0,0,1],fontsize=10,fmt='%i')
        parallels = np.arange(mS,mN,np.around((mN-mS)/10.0))
        m.drawparallels(parallels,labels=[1,0,0,0],fontsize=10,fmt='%i')
        markersize = 3
#         lonsm , latsm = np.meshgrid(lons,lats)
        x,y = m(lons,lats)
        # m.contourf(x,y,d_ws)
        
#         m.scatter(x,y,lw=0,s=3,c=d_ws)
        m.scatter(x,y,lw=0,s=3,c=a_times_num)
        plt.colorbar()
        plt.show()
    field_ws_a = nc.read_field('wind_speed')
    print "field_ws_a",field_ws_a
    # mygrid = Grid()
#     myfeat = Trajectory()
    myfeat = Trajectory(longitudes=lons,latitudes=lats,times=a_times,fields=collections.OrderedDict([('field_ws_a',field_ws_a)]))
#     mygrid = Swath(longitudes=lons,latitudes=lats,times=a_times,fields=collections.OrderedDict([('field_ws_a',field_ws_a)]))
    # mygrid.load(nc)
    print myfeat
    
def test_times_from_field_dt_or_num(nc):
    lats = nc.read_values('lat')
    lons = nc.read_values('lon')
    a_times = nc.read_values('time')
    print "a_times",a_times
    field_ws_a = nc.read_field('wind_speed')
    field_tie_mapper = nc.read_field('time')
    myfeat = Trajectory(longitudes=lons,latitudes=lats,times=field_tie_mapper,fields=collections.OrderedDict([('field_ws_a',field_ws_a)]))
    fieldt = myfeat.get_geolocation_field('time')
    values_ti = fieldt.get_values()
    print values_ti
if __name__ =='__main__':
    logging.basicConfig(level=logging.DEBUG)
    ff = '/home/cerdata/provider/remss/smap/l3/v00.1/daily/data/2017/031/RSS_smap_wind_daily_2017_01_31_v00.1.nc'
    nc = SMAPL3REMSSNCFile(ff,node='ascending')
#     test_loadind(nc)
#     test_times_from_field_dt_or_num(nc)
    test_minutes(nc)