"""

Test class for cerbere SMAP JPL L2

:copyright: Copyright 2017 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from cerbere.mapper.checker import Checker


class SMAPJPLHDF5FileChecker(Checker, unittest.TestCase):
    """Test class for SMAP JPL L2 files"""

    def __init__(self, methodName="runTest"):
        super(SMAPJPLHDF5FileChecker, self).__init__(methodName)

    @classmethod
    def mapper(cls):
        """Return the mapper class name"""
        return 'smapjplhdf5file.SMAPJPLHDF5File'

    @classmethod
    def datamodel(cls):
        """Return the related datamodel class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "SMAP_L2B_SSS_10523_20170120T013245_R14010.h5"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
